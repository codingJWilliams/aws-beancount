import aws_lambda_wsgi
from fava.application import app
import json

app.config["BEANCOUNT_FILES"] = ["/mnt/efs/ledger.beancount"]
app.config["INCOGNITO"] = False
app.config["READ_ONLY"] = False

with open("./params.json", "r") as f:
    params = json.loads(f.read())

def lambda_handler(event, context):
    if "X-Cloudfront-Auth" not in event['headers'] or event['headers']['X-Cloudfront-Auth'] != params['auth_key']:
        return {
            "isBase64Encoded": False,
            "statusCode": 403,
            "body": "Direct requests are not permitted"
        }
    return aws_lambda_wsgi.response(app, event, context)