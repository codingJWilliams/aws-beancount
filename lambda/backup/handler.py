import boto3, json

with open("./params.json") as f:
    params = json.loads(f.read())

s3 = boto3.client('s3')

def handler(event, context):
    with open("/mnt/efs/ledger.beancount", "rb") as ledger:
        res = s3.put_object(
            Body = ledger.read(),
            Bucket = params['ledger_bucket'],
            Key = "ledger.beancount"
        )
        print(res)

    return True