import boto3, json, os

with open("./params.json") as f:
    params = json.loads(f.read())

s3 = boto3.resource('s3')

def handler(event, context):
    s3.Bucket(params['ledger_bucket']).download_file('ledger.beancount', '/tmp/ledger.beancount')

    with open('/tmp/ledger.beancount', 'rb') as f1:
        with open('/mnt/efs/ledger.beancount', 'wb') as f2:
            f2.write(f1.read())
    return True