module "backup" {
  source = "./modules/timed-lambda"

  src      = "${path.module}/../lambda/backup"
  name     = "backup-ledger"
  context  = module.this
  schedule = "34 3 * * ? *"
  lambda = {
    handler = "handler.handler"
    runtime = "python3.9"
    vpc_config = {
      subnet_ids         = [data.aws_subnet.default.id]
      security_group_ids = [aws_security_group.backup.id]
    }
    file_system_config = {
      efs_access_point_arn = aws_efs_access_point.fava.arn
      local_mount_path     = "/mnt/efs"
    }
  }

  params = {
    ledger_bucket = aws_s3_bucket.ledger.bucket
  }

  function_policy = data.aws_iam_policy_document.backup.json
}

resource "aws_security_group" "backup" {
  name        = "${module.this.id}-backup"
  description = "Security group to allow egress for Backup"
  vpc_id      = data.aws_vpc.default.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${module.this.id}-fava"
  }
}

data "aws_iam_policy_document" "backup" {
  statement {
    actions = [
      "s3:PutObject"
    ]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.ledger.arn}/ledger.beancount"]
  }
  statement {
    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
      "ec2:AssignPrivateIpAddresses",
      "ec2:UnassignPrivateIpAddresses"
    ]
    effect    = "Allow"
    resources = ["*"]
  }
}

resource "aws_s3_bucket" "ledger" {
  bucket = "${module.this.id}-ledger"
}

resource "aws_s3_bucket_versioning" "ledger" {
  bucket = aws_s3_bucket.ledger.id
  versioning_configuration {
    status = "Enabled"
  }
}


data "aws_route_table" "selected" {
  vpc_id = data.aws_vpc.default.id
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id          = data.aws_vpc.default.id
  service_name    = "com.amazonaws.${var.region}.s3"
  route_table_ids = [data.aws_route_table.selected.id]
}

resource "aws_s3_bucket_lifecycle_configuration" "ledger" {
  bucket = aws_s3_bucket.ledger.id

  rule {
    id     = "delete-old"
    status = "Enabled"
    expiration {
      days = 14
    }
  }
}

resource "aws_s3_bucket_acl" "ledger" {
  bucket = aws_s3_bucket.ledger.bucket
  acl    = "private"
}

resource "aws_s3_bucket_public_access_block" "ledger" {
  bucket = aws_s3_bucket.ledger.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
