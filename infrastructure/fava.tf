resource "local_file" "fava" {
  filename = "../lambda/fava/params.json"
  content = jsonencode({
    auth_key = random_password.auth.result
  })
}

resource "random_password" "auth" {
  length  = 48
  special = false
}

data "archive_file" "fava" {
  type        = "zip"
  source_dir  = "../lambda/fava/"
  output_path = "../lambda/fava.zip"
  depends_on = [
    local_file.fava
  ]
  excludes = [
    "__pycache__",
    "venv",
  ]
}

resource "aws_iam_role" "fava" {
  name                = "${module.this.id}-fava"
  managed_policy_arns = [aws_iam_policy.fava.arn, "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]

  assume_role_policy = data.aws_iam_policy_document.AssumeByLambda.json
}

data "aws_iam_policy_document" "fava" {
  statement {
    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface"
    ]
    effect    = "Allow"
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "AssumeByLambda" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "fava" {
  name        = "${module.this.id}-fava"
  description = "Policy to allow the fava lambda function to call S3 etc"

  policy = data.aws_iam_policy_document.fava.json
}

resource "aws_lambda_permission" "fava" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.fava.function_name
  principal     = "arn:aws:iam::${var.account_id}:root"
}

resource "aws_lambda_permission" "fava2" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.fava.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.fava.execution_arn}/*/*/{proxy+}"
}

resource "aws_lambda_function" "fava" {
  filename         = data.archive_file.fava.output_path
  function_name    = "${module.this.id}-fava"
  role             = aws_iam_role.fava.arn
  handler          = "handler.lambda_handler"
  source_code_hash = data.archive_file.fava.output_base64sha256
  layers           = [aws_lambda_layer_version.fava-layer.arn]
  runtime          = "python3.9"
  timeout          = 25
  architectures    = ["arm64"]

  vpc_config {
    # Every subnet should be able to reach an EFS mount target in the same Availability Zone. Cross-AZ mounts are not permitted.
    subnet_ids         = [data.aws_subnet.default.id]
    security_group_ids = [aws_security_group.default.id]
  }

  file_system_config {
    # EFS file system access point ARN
    arn = aws_efs_access_point.fava.arn

    # Local mount path inside the lambda function. Must start with '/mnt/'.
    local_mount_path = "/mnt/efs"
  }
}

resource "aws_security_group" "default" {
  name        = "${module.this.id}-fava"
  description = "Security group to allow egress for Fava"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.backup.id]
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${module.this.id}-fava"
  }
}

data "aws_subnet" "default" {
  availability_zone = "${var.region}a"
  default_for_az    = true
}

data "aws_vpc" "default" {
  default = true
}

resource "aws_efs_file_system" "fava" {
  creation_token         = "${module.this.id}-fava"
  encrypted              = true
  availability_zone_name = "${var.region}a"

  tags = {
    Name = "${module.this.id}-fava"
  }
}

# Mount target connects the file system to the subnet
resource "aws_efs_mount_target" "fava" {
  file_system_id  = aws_efs_file_system.fava.id
  subnet_id       = data.aws_subnet.default.id
  security_groups = [aws_security_group.default.id]
}

# EFS access point used by lambda file system
resource "aws_efs_access_point" "fava" {
  file_system_id = aws_efs_file_system.fava.id

  root_directory {
    path = "/ledgers"
    creation_info {
      owner_gid   = 1000
      owner_uid   = 1000
      permissions = "777"
    }
  }

  posix_user {
    gid = 1000
    uid = 1000
  }
}

resource "aws_lambda_layer_version" "fava-layer" {
  filename            = "../data/fava-dependencies.zip"
  layer_name          = "${module.this.id}-fava"
  source_code_hash    = filebase64sha256("${path.module}/../data/fava-dependencies.zip")
  compatible_runtimes = ["python3.9"]
}
