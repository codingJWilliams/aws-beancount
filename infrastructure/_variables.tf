variable "profile" {
  description = "AWS profile"
  type        = string
}

variable "region" {
  description = "AWS region to deploy to"
  type        = string
}

variable "account_id" {
  description = "AWS account ID"
  type        = string
}

variable "environment" {
  description = "Environment"
  type        = string
  default     = "prod"
}

variable "azure_location" {
  type        = string
  description = "Where to put Azure resources - az account list-locations | jq '.[].displayName'"
  default     = "UK South"
}

variable "cloudflare_api_token" {
  description = "Token to access Cloudflare API"
  type        = string
}

variable "domain" {
  description = "Domain to use "
  type        = string
}
variable "subdomain" {
  description = "Subdomain to host Fava on"
  type        = string
}