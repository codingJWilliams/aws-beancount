resource "aws_acm_certificate" "domain" {
  provider          = aws.us-east-1
  domain_name       = "${var.subdomain}.${var.domain}"
  validation_method = "DNS"
}

resource "aws_acm_certificate_validation" "validation" {
  provider        = aws.us-east-1
  certificate_arn = aws_acm_certificate.domain.arn

  validation_record_fqdns = [
    "${tolist(aws_acm_certificate.domain.domain_validation_options)[0].resource_record_name}"
  ]
}

resource "aws_acm_certificate" "domain-eu" {
  domain_name       = "${var.subdomain}.${var.domain}"
  validation_method = "DNS"
}

resource "aws_acm_certificate_validation" "validation-eu" {
  certificate_arn = aws_acm_certificate.domain-eu.arn

  validation_record_fqdns = [
    "${tolist(aws_acm_certificate.domain-eu.domain_validation_options)[0].resource_record_name}"
  ]
}

data "cloudflare_zone" "domain" {
  name = var.domain
}

resource "cloudflare_record" "validation" {
  zone_id = data.cloudflare_zone.domain.zone_id
  name    = tolist(aws_acm_certificate.domain.domain_validation_options)[0].resource_record_name
  value   = trimsuffix(tolist(aws_acm_certificate.domain.domain_validation_options)[0].resource_record_value, ".")
  type    = tolist(aws_acm_certificate.domain.domain_validation_options)[0].resource_record_type
  ttl     = 60
}

resource "cloudflare_record" "cname" {
  zone_id = data.cloudflare_zone.domain.zone_id
  name    = var.subdomain
  value   = module.cdn.cf_domain_name
  type    = "CNAME"
  ttl     = 60
}