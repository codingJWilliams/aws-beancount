data "aws_cloudfront_cache_policy" "CachingDisabled" {
  name = "Managed-CachingDisabled"
}

data "aws_cloudfront_origin_request_policy" "AllViewer" {
  name = "Managed-AllViewer"
}

module "auth-gate" {
  source = "git::https://gitlab.com/littlewonders/terraform/cloudfront-azure-ad-authentication.git?ref=v1.0.1"

  context             = module.this
  name                = "cdn"
  application_name    = "Fava"
  cloudfront_endpoint = "https://${var.subdomain}.${var.domain}"
  any_origin          = "${module.this.id}-cdn"

  region  = var.region
  profile = var.profile
}

module "cdn" {
  source = "cloudposse/cloudfront-cdn/aws"

  name    = "cdn"
  context = module.this

  #aliases = var.backend_own_domain ? ["${(var.backend_subdomain != "" ? "${var.backend_subdomain}." : "") + var.backend_domain}"] : []
  aliases             = ["${var.subdomain}.${var.domain}"]
  dns_aliases_enabled = false
  acm_certificate_arn = aws_acm_certificate.domain.arn
  logging_enabled     = false
  default_root_object = ""
  allowed_methods     = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]

  cached_methods = ["GET", "HEAD"]

  cache_policy_id          = data.aws_cloudfront_cache_policy.CachingDisabled.id
  origin_request_policy_id = data.aws_cloudfront_origin_request_policy.AllViewer.id

  default_ttl        = 0 # Overridden by cache policy
  max_ttl            = 0 # Overridden by cache policy
  origin_domain_name = replace(aws_apigatewayv2_api.fava.api_endpoint, "https://", "")
  custom_header = [{
    name  = "X-Cloudfront-Auth"
    value = random_password.auth.result
  }]
  origin_http_port       = 80
  origin_https_port      = 443
  origin_protocol_policy = "https-only"
  custom_error_response  = [/*{
    error_code            = 403
    error_caching_min_ttl = 0
    response_page_path    = ""
    response_code         = 0
  }*/]
  //block_origin_public_access_enabled = true
  comment = "Beancount ${var.environment}"
  function_association = [
    {
      event_type   = "viewer-request"
      function_arn = module.auth-gate.function_id
    }
  ]
  ordered_cache = [
    module.auth-gate.cache_behaviour
  ]
}
