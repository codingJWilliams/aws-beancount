resource "local_file" "restore" {
  filename = "${path.module}/../lambda/restore/params.json"
  content = jsonencode({
    ledger_bucket = aws_s3_bucket.ledger.bucket
  })
}

data "archive_file" "restore" {
  type        = "zip"
  source_dir  = "${path.module}/../lambda/restore"
  output_path = "${path.module}/../lambda/restore.zip"
  depends_on = [
    local_file.restore
  ]
  excludes = [
    "__pycache__",
    "venv",
  ]
}

resource "aws_iam_role" "restore" {
  name                = "${module.this.id}-restore-lambda"
  managed_policy_arns = [aws_iam_policy.restore2.arn, "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]

  assume_role_policy = data.aws_iam_policy_document.restore.json
}

data "aws_iam_policy_document" "restore" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "restore2" {
  name        = "${module.this.id}-restore-lambda"
  description = "Permissions for the lambda itself"

  policy = data.aws_iam_policy_document.restore2.json
}

data "aws_iam_policy_document" "restore2" {
  statement {
    actions = [
      "s3:GetObject"
    ]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.ledger.arn}/ledger.beancount"]
  }
  statement {
    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
      "ec2:AssignPrivateIpAddresses",
      "ec2:UnassignPrivateIpAddresses"
    ]
    effect    = "Allow"
    resources = ["*"]
  }
}

resource "aws_lambda_permission" "restore" {
  action        = "lambda:InvokeFunction"
  function_name = module.restore.name
  principal     = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
}

data "aws_caller_identity" "current" {}

module "restore" {
  source  = "terraform-module/lambda/aws"
  version = "~> 2"

  function_name    = "${module.this.id}-restore"
  filename         = data.archive_file.restore.output_path
  source_code_hash = data.archive_file.restore.output_base64sha256
  description      = module.this.id
  handler          = "handler.handler"
  runtime          = "python3.9"
  memory_size      = "128"
  concurrency      = -1
  lambda_timeout   = "60"
  log_retention    = "1"
  role_arn         = aws_iam_role.restore.arn

  vpc_config = {
    subnet_ids         = [data.aws_subnet.default.id]
    security_group_ids = [aws_security_group.backup.id]
  }
  file_system_config = {
    efs_access_point_arn = aws_efs_access_point.fava.arn
    local_mount_path     = "/mnt/efs"
  }
}
